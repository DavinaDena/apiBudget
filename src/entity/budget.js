export class Budget{
    date;
    category;
    money;
    id;
    constructor(date,category,money,id){
        this.date= date;
        this.category= category;
        this.money= money;
        this.id= id;
    }
}