import express from 'express';
import { budget } from './controller/budgetController';
import cors from 'cors';



export const server = express();

server.use(cors() )


server.use(express.json());

server.use('/api/budget', budget);