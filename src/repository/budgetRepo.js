import { Budget } from "../entity/budget";
import { connection } from "./connection";


export class BudgetRepository {


    /**
     * 
     * @returns all the budget
     */
    static async getAllBudgetInfo() {
        const [rows] = await connection.execute('SELECT * FROM budget');
        let theBudget = [];
        for (let item of rows) {
            let budg = new Budget(item.date, item.category, item.money, item.id);
            theBudget.push(budg);
        }
        return theBudget;
    }


    static async searchBudget(term){
        const [rows] = await connection.execute('SELECT * FROM budget WHERE CONCAT (date,category,money) LIKE ?', ['%'+term+'%'])
        return rows.map(row => new Budget(row['date'], row['category'], row['money'], row['id']))
    }

    /**
     * 
     * @param {add} add
     * to add money 
     */
    static async addBudget(add) {
        const [rows] = await connection.execute('INSERT INTO budget (date, category, money) VALUES (?,?,?)', [add.date, add.category, add.money])
        add.id = rows.insertId
        return add
    }
    /**
     * 
     * @param {id} id
     * to delete a statement 
     */
    static async deleteBudget(id) {
        await connection.execute('DELETE FROM budget WHERE id=?', [id])
    }

    /**
     * 
     * @param {budget} up
     * to update a statement 
     */
    static async updateBudget(up) {
        await connection.execute('UPDATE budget SET date=?, category=?, money=? WHERE id=?', [up.date, up.category, up.money, up.id])
    }

    /**
     * 
     * @param {id} id 
     * @returns budget with matching id
     */
    static async findById(id){
        const [rows] = await connection.execute('SELECT * FROM budget WHERE id=?', [id])
        return new Budget(rows[0].date, rows[0].category, rows[0].money, rows[0].id)
    }

    /**
     * 
     * @param {month} month 
     * @returns the statements of that month
     */
    static async getOneMonth(month) {
        let [rows] = await connection.execute('SELECT * FROM budget WHERE MONTH(date)=?', [month]);
        let operations = [];
        for (let item of rows) {
            let operation = new Budget(item.date, item.category, item.money, item.id);
            operations.push(operation);
        }
        return operations;
    }

    // /**
    //  * 
    //  * @param {money} money 
    //  * @returns the statements with the same quantity of moneys
    //  */
    //  static async getBudgetByAmmount(money) {
    //     const [rows] = await connection.execute('SELECT * FROM budget WHERE money=?', [money])
    //     let budgetMo = [];
    //     for (let item of rows) {
    //         let budgetMoney = new Budget(item.date, item.category, item.money, item.id);
    //         budgetMo.push(budgetMoney);
    //     }
    //     return budgetMo;
    // }

    // /**
    //  * 
    //  * @param {category} cat 
    //  * @returns the budget by category
    //  */
    //  static async getBudgetCategory(cat) {
    //     const [rows] = await connection.execute('SELECT * FROM budget WHERE category=?', [cat]);
    //     let budgetCat = [];
    //     for (let item of rows) {
    //         let budgetCategory = new Budget(item.date, item.category, item.money, item.id);
    //         budgetCat.push(budgetCategory);
    //     }
    //     return budgetCat;
    // }



}