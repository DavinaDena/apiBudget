import { Router } from "express";
import { BudgetRepository } from "../repository/budgetRepo";


export const budget = Router();
// we need to change the /all etc, see jean doc

/**
 * get all statements
 */
budget.get('/', async (req, res) => {

    try {
        let budget;
        if (req.query.search) {
            budget = await BudgetRepository.searchBudget(req.query.search)
        } else {
            budget = await BudgetRepository.getAllBudgetInfo()
        }
        res.json(budget)
    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }

})

//get by id
budget.get('/:id', async (req,res)=>{
    try {
        let data = await BudgetRepository.findById(req.params.id);
        res.json(data);
    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }
})
/**
 * get a statement by category
 */
// budget.get('/:cat', async (req, res) => {

//     try {
//         let budg = await BudgetRepository.getBudgetCategory(req.params.cat);
//         if (!budg) {
//             res.status(404).json({ error: 'Not Found' })
//             return
//         }
//         res.json(budg)
//     } catch (error) {
//         res.status(500).json(error)
//     }
// })

/**
 * get a statement with equal amout of money
 */
// budget.get('/:money', async (req, res) => {
//     let budg = await BudgetRepository.getBudgetByAmmount(req.params.money)
//     res.json(budg)
// })

/**
 * to add a statement
 */
budget.post('/', async (req, res) => {
    try {
        let budg = await BudgetRepository.addBudget(req.body)
        res.status(201).json(budg)
    } catch (error) {
        res.status(500).json(error)
    }
})

/**
 * to delete a statement
 */
budget.delete('/:id', async (req, res) => {
    try {
        await BudgetRepository.deleteBudget(req.params.id)
        res.status(204).end()
    } catch (error) {
        res.status(500).json(error)
    }
})



//to change
/**
 * to update a statement
 */
budget.patch('/:id', async (req, res) => {
    try {
        let data = await BudgetRepository.findById(req.params.id);
        if (!data) {
            res.send(404).end();
            return
        }
        let update = { ...data, ...req.body };
        await BudgetRepository.updateBudget(update);
        res.json(update);
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

/**
 * to get the statements on X month
 */
budget.get('/month/:month', async (req, res) => {
    let budg = await BudgetRepository.getOneMonth(req.params.month)
    res.json(budg)
})



