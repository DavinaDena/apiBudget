
DROP TABLE IF EXISTS budget;
CREATE TABLE budget (
    id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
    date DATE,
    category VARCHAR (40),
    money FLOAT
);

INSERT INTO budget (date,category,money) VALUE (
    '2021-07-29', 'Salary', '1507.50'
), 
('2020-03-04', 'Restaurant', '-13.50');

INSERT INTO budget (date,category,money) VALUE (
    '2021-07-30', 'Beer', '7.50'
);